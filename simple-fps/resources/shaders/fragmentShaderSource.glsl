/*#version 330 core
out vec4 FragColor;
//Gets the input from the vert shader
in vec3 vertexColor = vec3(1, 1, 1);
in vec2 texCoord;

uniform sampler2D _MainTex;

uniform float time;

float rand(vec2 n) { 
	return fract(sin(dot(n, vec2(12.9898, 4.1414))) * 43758.5453);
}

float noise(vec2 p){
	vec2 ip = floor(p);
	vec2 u = fract(p);
	u = u*u*(3.0-2.0*u);
	
	float res = mix(
		mix(rand(ip),rand(ip+vec2(1.0,0.0)),u.x),
		mix(rand(ip+vec2(0.0,1.0)),rand(ip+vec2(1.0,1.0)),u.x),u.y);
	return res*res;
}

void main()
{
   //FragColor.rgb = vertexColor.rgb;
   vec2 position = (texCoord * 30);
   position += (time * 5);
   //position += ((noise(gl_FragCoord.xy / 8) + 1) / 2);
   FragColor.rgb = vec3((sin(position) + 1) / 2, 0);
   FragColor.b = 1;

   FragColor.rgb = texture(_MainTex, texCoord * vec2(1, -1)).rgb* vertexColor;
}
*/

#version 330 core

// Interpolated values from the vertex shaders
in vec2 UV;

// Ouput data
out vec3 color;

// Values that stay constant for the whole mesh.
uniform sampler2D myTextureSampler;

void main(){

	// Output color = color of the texture at the specified UV
	color = texture( myTextureSampler, UV ).rgb;
}