#version 330 core

// Interpolated values from the vertex shaders
in vec2 UV;
in vec3 Position_worldspace;
in vec3 Normal_cameraspace;
in vec3 EyeDirection_cameraspace;
in vec3 LightDirection_cameraspace;

// Ouput data
out vec4 color;

// Values that stay constant for the whole mesh.
uniform sampler2D diffuse_sampler;
uniform sampler2D emissive_sampler;
uniform float clip;
uniform float alpha;
uniform vec3 emission;
uniform vec3 specular;
uniform mat4 MVP;
uniform mat4 V;
uniform mat4 M;
uniform vec3 LightPosition_worldspace;
uniform vec3 LightColor;
uniform float LightPower;
void main(){
	// Material properties
	vec3 MaterialDiffuseColor = texture( diffuse_sampler, UV ).rgb;
	vec3 MaterialEmissiveColor = texture( emissive_sampler, UV ).rgb;
	vec3 MaterialAmbientColor = vec3(0.5,0.5,0.5) * MaterialDiffuseColor;

	vec3 out_emission = emission * MaterialDiffuseColor;
	//vec3 MaterialSpecularColor = vec3(0.3,0.3,0.3);
	//Clip early if possible
	if(clip != 0 && MaterialDiffuseColor.r <= 0.001)
		discard;
	// Distance to the light
	float distance = length( LightPosition_worldspace - Position_worldspace );

	// Normal of the computed fragment, in camera space
	vec3 n = normalize( Normal_cameraspace );
	// Direction of the light (from the fragment to the light)
	vec3 l = normalize( LightDirection_cameraspace );
	// Cosine of the angle between the normal and the light direction, 
	// clamped above 0
	//  - light is at the vertical of the triangle -> 1
	//  - light is perpendicular to the triangle -> 0
	//  - light is behind the triangle -> 0
	float cosTheta = clamp( dot( n,l ), 0,1 );
	
	// Eye vector (towards the camera)
	vec3 E = normalize(EyeDirection_cameraspace);
	// Direction in which the triangle reflects the light
	vec3 R = reflect(-l,n);
	// Cosine of the angle between the Eye vector and the Reflect vector,
	// clamped to 0
	//  - Looking into the reflection -> 1
	//  - Looking elsewhere -> < 1
	float cosAlpha = clamp( dot( E,R ), 0,1 );
	
	vec3 color_noalpha = 
		// Ambient : simulates indirect lighting
		MaterialAmbientColor +
		// Diffuse : "color" of the object
		MaterialDiffuseColor * LightColor * LightPower * cosTheta / (distance*distance*distance) +
		// Specular : reflective highlight, like a mirror
		clamp(specular, 0, 1) * LightColor * LightPower * pow(cosAlpha,5) / (distance*distance*distance);
	color = vec4(color_noalpha.x, color_noalpha.y, color_noalpha.z, alpha);
	color = max(color, vec4(out_emission + MaterialAmbientColor, 0));

}