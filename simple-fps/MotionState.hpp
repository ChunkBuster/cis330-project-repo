#pragma once
#include "LinearMath/btTransform.h"
#include "LinearMath/btDefaultMotionState.h"

class MotionState : public btDefaultMotionState {

public:
	MotionState(const btTransform & transform) : btDefaultMotionState(transform) {
	}

	void getModelMatrix(btScalar * transform) const {
		btTransform modelMatrix;

		getWorldTransform(modelMatrix);
		modelMatrix.getOpenGLMatrix(transform);
	}
};
