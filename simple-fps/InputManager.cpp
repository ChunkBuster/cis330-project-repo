﻿#include "stdafx.h"
#include "fwd.h"
#include "glyphs.h"
#include "imgui_console.hpp"

//#include "coroutines.hpp"

#include "InputManager.hpp"
#include "Transform.hpp"
#include "Weapon.hpp"

#include "Logger.hpp"
//#include "Exawon/CoroBehaviour.h"
//#include "Exawon/CoroDefinition.h"

//#include "split_stream.hpp"

//using namespace emilib::cr;
ImGuiIO * InputManager::io = nullptr;
glm::mat4 InputManager::viewMatrix(translate(glm::mat4(1), glm::vec3(0.0f, 0.0f, -6.0f)));
glm::vec3 InputManager::position(0);
glm::vec2 InputManager::sensitivity(0.05, 0.1);
glm::vec2 InputManager::moveSpeed(1, 1);
double InputManager::xRot(0);
double InputManager::yRot(0);
glm::vec2 InputManager::screenMiddle(0);
glm::vec2 InputManager::screenSize(0);
double InputManager::lastTime = glfwGetTime();
double InputManager::delta_time;

transform InputManager::player_transform = transform();
boost::asio::io_service InputManager::ioService;
Weapon * InputManager::player_gun = nullptr;
//std::vector<std::shared_ptr<Coroutine>> input_manager::running_coroutines = std::vector<std::shared_ptr<Coroutine>>();

bool cursorLocked = true;

void InputManager::setCursorLock(GLFWwindow * window, bool lock) {
	//Hide the cursor
	if (lock)
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	else
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	glfwSetCursorPos(window, screenMiddle.x, screenMiddle.y);
	cursorLocked = lock;
}


//This is a method that is called when the viewport is resized, set to a callback.
void framebuffer_size_callback(GLFWwindow * window, int width, int height) {
	glViewport(0, 0, width, height);
	glfwGetWindowSize(window, &width, &height);
	InputManager::screenSize = glm::vec2(width, height);
	InputManager::screenMiddle = glm::vec2(width / 2, height / 2);
}

void key_callback(GLFWwindow * window, int key, int scancode, int action, int mods) {
	ImGui_ImplGlfw_KeyCallback(window, key, scancode, action, mods);
	//input_manager::io.
}


void mouse_callback(GLFWwindow * window, int key, int action, int mods) {
	InputManager::player_gun->poll(key, action);
	ImGui_ImplGlfw_MouseButtonCallback(window, key, action, mods);
}

/*
std::shared_ptr<Coroutine> input_manager::start_coroutine(const char* debug_name, std::function<void(emilib::cr::InnerControl &c)> fun)
{
	std::shared_ptr<Coroutine> coroutine = coroutine_set.start(debug_name, fun);
	running_coroutines.push_back(coroutine);
	return coroutine;
}*/
static bool closing = false;

void InputManager::init_ui(GLFWwindow * window) {
	player_gun = new Weapon();
#if IMGUI
	ImGui::CreateContext();
	ImGui_ImplGlfwGL3_Init(window, true);
	// Setup style
	ImGui::StyleColorsDark();
	io = &ImGui::GetIO();
	(void)io;
	//io.Fonts->GetGlyphRangesDefault()
	io->Fonts->AddFontFromFileTTF("menlo_powerline.ttf", 24, nullptr, glyph_range);
	io->Fonts->Build();
#endif
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	//_GLFW_REQUIRE_INIT_OR_RETURN(NULL);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	//Doesn't work?
	glfwSetMouseButtonCallback(window, mouse_callback);
	glfwSetKeyCallback(window, key_callback);
	//Forcibly call the method to initialize logic
	framebuffer_size_callback(window, WINDOW_WIDTH, WINDOW_HEIGHT);
}

//Processes input from the user
void InputManager::process_input(GLFWwindow * window) {
	static int oldState = GLFW_RELEASE;
	int newState = glfwGetKey(window, GLFW_KEY_GRAVE_ACCENT);
	if (newState == GLFW_PRESS && oldState == GLFW_RELEASE) {
		if (glfwGetKey(window, GLFW_KEY_GRAVE_ACCENT) == GLFW_PRESS) {
			setCursorLock(window, !cursorLocked);
			oldState = newState;
			return;
		}

	}
	oldState = newState;

	//Hide the cursor
	if (cursorLocked)
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	else
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);


	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, true);
		closing = true;
	}


	//If cursor is not locked, exit
	if (!cursorLocked) return;


	//Get screen data
	int width = 0;
	int height = 0;

	//Get mouse positioning data
	double xPos = 0;
	double yPos = 0;
	glfwGetCursorPos(window, &xPos, &yPos);

	const glm::vec2 deltaPos(screenMiddle.x - xPos, screenMiddle.y - yPos);
	//Get time data
	double currentTime = glfwGetTime();
	delta_time = (currentTime - lastTime);
	lastTime = currentTime;

	//Camera Rotation

	xRot += deltaPos.x * sensitivity.x * delta_time;
	yRot += deltaPos.y * sensitivity.y * delta_time;

	// Direction : Spherical coordinates to Cartesian coordinates conversion
	glm::vec3 direction(
		cos(yRot) * sin(xRot),
		sin(yRot),
		cos(yRot) * cos(xRot)
	);

	// Right vector
	glm::vec3 right = glm::vec3(
		sin(xRot - glm::pi<double>() / 2.0),
		0,
		cos(xRot - glm::pi<double>() / 2.0)
	);

	// Up vector
	glm::vec3 up = cross(right, direction);

	glm::vec2 inputVec(0);

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		inputVec.y -= 1;

	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		inputVec.y += 1;

	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		inputVec.x += 1;

	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		inputVec.x -= 1;

	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
		inputVec *= 4;

	inputVec *= moveSpeed * static_cast<float>(delta_time);
	glm::vec3 moveVec(player_transform.rotation * glm::vec3(inputVec.x, 0, inputVec.y));
	position += moveVec;
	viewMatrix = lookAt(
		position, // Camera is here
		position + direction, // and looks here : at the same position, plus "direction"
		up // Head is up (set to 0,-1,0 to look upside-down)
	);

	glm::vec3 scale;
	glm::quat rotation;
	glm::vec3 translation;
	glm::vec3 skew;
	glm::vec4 perspective;
	decompose(viewMatrix, scale, rotation, translation, skew, perspective);

	player_transform.position = -translation; //position;
	player_transform.rotation = conjugate(toQuat(viewMatrix));

	ioService.poll();

	glfwSetCursorPos(window, screenMiddle.x, screenMiddle.y);
}


void InputManager::draw_imgui() {
	if (cursorLocked) return;
#if IMGUI
	ImGui_ImplGlfwGL3_NewFrame();
	Logger::draw_console();
	//ImGui::ShowDemoWindow();
	ImGui::Render();
#endif
}
