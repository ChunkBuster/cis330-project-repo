﻿#pragma once
#include "fwd.h"

class transform {
public:
	transform();
	transform(transform * parent);
	transform * parent;

	glm::vec3 scale;
	glm::quat rotation;
	glm::vec3 position;
	glm::vec3 skew;
	glm::vec4 perspective;

	glm::vec3 global_position() const;

	glm::vec3 up() const;
	glm::vec3 right() const;
	glm::vec3 down() const;
	glm::vec3 left() const;
	glm::vec3 forward() const;
	glm::vec3 back() const;

	glm::mat4 get_matrix() const;
	glm::mat4 get_matrix_global() const;

	void set_matrix(glm::mat4 matrix);
	void set_to_rb(btRigidBody & body);
};
