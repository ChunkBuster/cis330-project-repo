#include "stdafx.h"

#include "InputManager.hpp"
#include "Transform.hpp"
#include "MaterialManager.hpp"
#include "TextureManager.hpp"
#include "Renderer.hpp"

renderer::renderer(GLuint VAO, GLuint VBO, GLuint UVBO, GLuint NBO, GLuint EBO, ShaderManager * shader,
                   std::vector<MeshManager::sub_mesh> indexBufferVec) :
	opaque_sub_meshes(), transp_sub_meshes(), VAO(VAO), VBO(VBO), UVBO(UVBO), NBO(NBO), EBO(EBO), shader(shader) {
	transf = new transform();
	for (const auto & sub : indexBufferVec) {
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sub.EBO);

		if (sub.mat->opacity == 1 || sub.mat->clip)
			opaque_sub_meshes.push_back(sub);
		else
			transp_sub_meshes.push_back(sub);
	}
}


renderer::~renderer() {

}

void renderer::render() {
	float timeValue = glfwGetTime();
#if LIGHT_MAP
	//Begin shadow map bullshit----------------------------------
	// Render to our framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);
	glViewport(0, 0, SHADOWMAP_SIZE, SHADOWMAP_SIZE); // Render on the whole framebuffer, complete from the lower left corner to the upper right

	// We don't use bias in the shader, but instead we draw back faces, 
	// which are already separated from the front faces by a small distance 
													  // (if your geometry is made this way)
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK); // Cull back-facing triangles -> draw only front-facing triangles

						 // Use our shader
	glUseProgram(DepthProgramID);

	glm::vec3 lightInvDir = glm::vec3(0.5, 2, 2);
	//lightInvDir = glm::quat(glm::vec3(0, 0, glm::radians(sin(timeValue) * 100.0f))) * lightInvDir;
	// Compute the MVP matrix from the light's point of view
	const int scale = 20;
	glm::mat4 depthProjectionMatrix = glm::ortho<float>(-scale, scale, -scale, scale, -scale, 2 * scale);
	glm::mat4 depthViewMatrix = glm::lookAt(lightInvDir, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	//depthViewMatrix = glm::rotate(depthViewMatrix, glm::radians(sin(timeValue) * 100.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	// or, for spot light :
	//glm::vec3 lightPos(5, 20, 20);
	//glm::mat4 depthProjectionMatrix = glm::perspective<float>(45.0f, 1.0f, 2.0f, 50.0f);
	//glm::mat4 depthViewMatrix = glm::lookAt(lightPos, lightPos-lightInvDir, glm::vec3(0,1,0));

	glm::mat4 depthModelMatrix = glm::mat4(1.0);
	glm::mat4 depthMVP = depthProjectionMatrix * depthViewMatrix * depthModelMatrix;
	// Get a handle for our "MVP" uniform

	// Send our transformation to the currently bound shader, 
	// in the "MVP" uniform
	glUniformMatrix4fv(DepthMatrixID, 1, GL_FALSE, &depthMVP[0][0]);

	// Vertex Attribute Buffer
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);


	// Index buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);

	// Draw the depth map
	glDrawElements(
		GL_TRIANGLES,      // mode
		indices.size(),    // count
		GL_UNSIGNED_INT, // type
		(void*)0           // element array buffer offset
	);

	glDisableVertexAttribArray(0);

#endif


	/*glVertexAttribPointer(
	0,                  // attribute
	3,                  // size
	GL_FLOAT,           // type
	GL_FALSE,           // normalized
	0,                  // stride
	(void*)0            // array buffer offset
	);*/


	//Get the sampler location
	glUniform1i(shader->get_uniform("diffuse_sampler"), 0);
	glActiveTexture(GL_TEXTURE0);

	//glUniform1i(shader->get_uniform("emissive_sampler"), 1);
	//glActiveTexture(GL_TEXTURE1);
	//Begin uniform binding ------------------------
	float greenValue = (sin(timeValue) / 2.0f) + 0.5f;

	glUseProgram(shader->shaderProgram);

	glm::mat4 proj = glm::perspective(glm::radians(80.0f), 1.0f, 0.1f, 100.0f);
	glm::mat4 view = InputManager::viewMatrix;
	//Bump a custom color value to the shader
	//unsigned int customColorLoc = glGetUniformLocation(shader->shaderProgram, "customColor");
	//glUniform4f(customColorLoc, 0.0f, greenValue, 0.0f, 1.0f);
#if LIGHT_MAP
	glm::mat4 biasMatrix(
		0.5, 0.0, 0.0, 0.0,
		0.0, 0.5, 0.0, 0.0,
		0.0, 0.0, 0.5, 0.0,
		0.5, 0.5, 0.5, 1.0
	);
	glm::mat4 depthBiasMVP = biasMatrix*depthMVP;
	glUniformMatrix4fv(DepthBiasID, 1, GL_FALSE, &depthBiasMVP[0][0]);

	glUniform3f(LightInvDirID, lightInvDir.x, lightInvDir.y, lightInvDir.z);
#endif
	glm::mat4 model = transf->get_matrix_global();
	glm::mat4 MV = view * model;
	glm::mat4 MVP = proj * view * model;

	glUniformMatrix4fv(shader->get_uniform("M"), 1, GL_FALSE, &model[0][0]);
	glUniformMatrix4fv(shader->get_uniform("V"), 1, GL_FALSE, &view[0][0]);
	glUniformMatrix4fv(shader->get_uniform("MV"), 1, GL_FALSE, &MV[0][0]);
	glUniformMatrix4fv(shader->get_uniform("MVP"), 1, GL_FALSE, &MVP[0][0]);

	//End uniform binding --------------------------


	// Vertex Attribute Buffer
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

	// UV Attribute Buffer
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, UVBO);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

	// Normal Attribute Buffer
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, NBO);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_CULL_FACE);

	for (const auto & sub : opaque_sub_meshes)
		render_sub_mesh(sub);

	glEnable(GL_BLEND);
	glDisable(GL_CULL_FACE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	for (const auto & sub : transp_sub_meshes)
		render_sub_mesh(sub);

	//Release bound attribute buffers
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}

void renderer::render_sub_mesh(MeshManager::sub_mesh sub) const {
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sub.EBO);
	if (sub.mat->diffus_map)
		glBindTexture(GL_TEXTURE_2D, sub.mat->diffus_map->texture);
	glUniform1f(shader->get_uniform("clip"), sub.mat->clip ? 1 : 0);
	glUniform1f(shader->get_uniform("alpha"), sub.mat->opacity);
	glUniformColor(shader->get_uniform("emission"), sub.mat->emissive_color);
	glUniformColor(shader->get_uniform("specular"), sub.mat->spec_color / (sub.mat->spec_exponent));
	glDrawElements(GL_TRIANGLES, sub.indices.size(), GL_UNSIGNED_INT, nullptr);
}
