﻿#pragma once
#include <vector>
#include <glm/detail/type_vec4.hpp>
#include <glm/detail/type_vec3.hpp>
#include <glm/detail/type_vec2.hpp>
#include <glm/detail/type_mat4x4.hpp>
#include <GL/glew.h>
#include <glm/common.hpp>

#define LOG_ERROR_SYMBOL u8"✗"
#define LOG_MESSAGE_SYMBOL u8" "
#define LOG_WARNING_SYMBOL u8"△"
#define LOG_COMMENT_SYMBOL u8"♯"

enum LogType {
	Comment,
	Message,
	Error,
	Warning,
	Special
};

#define WINDOW_WIDTH 1600
#define WINDOW_HEIGHT 1200

#define IMGUI 1
#define IMGUI_INPUT 1

#define VERBOSE_RESOURCES 0
#define TIME_CALL(x) \
  do { \
    	std::clock_t start; \
		double duration; \
		start = std::clock(); \
		x ;\
		duration = (std::clock() - start) / (double) CLOCKS_PER_SEC; \
		LOG(fmt::format("Time for \"(" #x ")\":{} seconds.", duration), LogType::Warning);\
  } while (0)

typedef unsigned idxVal;
typedef std::vector<idxVal> idxvec;

inline std::istream& operator >>(std::istream & in, glm::vec2 & v) {
	in >> v.x;
	in >> v.y;
	return in;
}

inline std::istream& operator >>(std::istream & in, glm::vec3 & v) {
	in >> v.x;
	in >> v.y;
	in >> v.z;
	return in;
}

inline std::istream& operator >>(std::istream & in, glm::vec4 & v) {
	in >> v.x;
	in >> v.y;
	in >> v.z;
	in >> v.w;
	return in;
}

inline void glUniformColor(const GLint location, const glm::vec3 color) {
	glUniform3f(location, color.x, color.y, color.z);
}

inline float lerp_floats(const float a, const float b, float time) {
	time = glm::clamp(time, 0.0f, 1.0f);
	return a * (1 - time) + b * time;
}

inline glm::vec3 lerp_vec3(const glm::vec3 A, const glm::vec3 B, float t) {
	return A * t + B * (1.f - t);
}

inline glm::mat4 btScalar2glmMat4(btScalar * matrix) {
	return glm::mat4(
		matrix[0], matrix[1], matrix[2], matrix[3],
		matrix[4], matrix[5], matrix[6], matrix[7],
		matrix[8], matrix[9], matrix[10], matrix[11],
		matrix[12], matrix[13], matrix[14], matrix[15]);
}
