﻿#include "stdafx.h"
#include "TextureManager.hpp"
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
// Include GLEW
#include <GL/glew.h>


texture_manager::texture_manager(std::string name) :
	ResourceManager<texture_manager>(name), texture(0), width(0), height(0), channels(0) {

}


std::string texture_manager::dir() {
	return ResourceManager<texture_manager>::dir() + "textures/";
}

bool texture_manager::loadResource(std::string name, bool mt) {
	std::string location = dir() + name;

	//Load the image data
	imageData = stbi_load(location.c_str(), &width, &height, &channels, 0);

	//Checks to make sure that the data was actually read
	if (!imageData) {
		LOG(fmt::format("Failed to load image file {}", name), LogType::Error);
		stbi_image_free(imageData);
		return false;
	}
#if VERBOSE_RESOURCES
		LOG(fmt::format("Loaded image {} ({}px by {}px, {}channels)", name, width, height, channels), LogType::Comment, u8"🎨");
#endif
	if (!mt)
		init_image();
	return true;
}

texture_manager::~texture_manager() {
}

void texture_manager::init_image(bool mt) {
	//takes two args- one, for how many images to load, Two, an array of pointers in which to store.
	glGenTextures(1, &texture);
	//Binds the texture so that subsequent calls will modify this texture
	glBindTexture(GL_TEXTURE_2D, texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	const GLenum format = channels == 3 ? GL_RGB : GL_RGBA;

	//Turns the raw image data into an OpenGL Texture. We pass in the width and Height values
	//obtained earlier. We specify a format (which defines how the image data is written), as well as 
	//a second format which is used for the input texture value.
	//The first integer is the LOD of the texture, while the second integer specifies a border of pixel data to not be read.
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, format, GL_UNSIGNED_BYTE, imageData);
	//Generate mip maps.
	glGenerateMipmap(GL_TEXTURE_2D);
	//Free the image data now that we are done with it
	//stbi_image_free(imageData);
}
