#include "stdafx.h"
#if SPLIT_STREAM
#include "stream_override.hpp"
std::streamsize stream_override::xsputn(const char_type* s, std::streamsize n)
{
	return std::streambuf::xsputn(s, n);
}

std::streambuf::int_type stream_override::overflow(int_type ch) 
{
	return std::streambuf::overflow(ch);
}
#endif
