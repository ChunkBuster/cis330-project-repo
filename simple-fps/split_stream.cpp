#include "stdafx.h"
#if SPLIT_STREAM
#include "split_stream.hpp"

split_stream *split_stream::split = new split_stream();

inline split_stream::split_stream() : 
ovr(), 
of_stream_split(),
old_cout(std::cout.rdbuf(&ovr)),
backup_cout(old_cout)
{
	ovr.of_stream_split = &of_stream_split;
	ovr.backup_cout = &backup_cout;
}

void split_stream::split_cout()
{
	split = new split_stream();
}

void split_stream::join_cout()
{
	std::cout.rdbuf(split->old_cout);
	delete(split);
}
#endif
