#include "stdafx.h"
#include "PhysObj.hpp"
#include "MeshManager.hpp"

#include "PhysWorld.hpp"

PhysicsObject::PhysicsObject(bool isStatic) {

}

PhysicsObject::PhysicsObject(glm::vec3 origin, float radius) {
	load_dynamic_sphere(origin, radius);
}

PhysicsObject::~PhysicsObject() {

}

void PhysicsObject::load_dynamic_sphere(glm::vec3 origin, float radius) {
	dynamic = true;
	collision_shape = new btSphereShape(radius);

	//Init ball
	motion_state = new btDefaultMotionState
		(btTransform(btQuaternion(0, 0, 0, 1), btVector3(origin.x, origin.y, origin.z)));
	//Give ball physical properties, like mass
	btScalar mass = 1;
	btVector3 fallInertia(0, 0, 0);
	collision_shape->calculateLocalInertia(mass, fallInertia);
	const btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(mass, motion_state, collision_shape, fallInertia);
	rb = new btRigidBody(fallRigidBodyCI);
	rb->setFriction(10.0f);
	PhysWorld::register_rigidbody(rb);
}

void PhysicsObject::reposition(btVector3 position, btQuaternion orientation) const {
	btTransform initialTransform;

	initialTransform.setOrigin(position);
	initialTransform.setRotation(orientation);

	rb->setWorldTransform(initialTransform);
	motion_state->setWorldTransform(initialTransform);
}
