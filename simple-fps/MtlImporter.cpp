#include "stdafx.h"
#include "MtlImporter.hpp"

#include "MeshManager.hpp"
#include "TextureManager.hpp"
#include "MaterialManager.hpp"
#include "MeshImportUtils.hpp"

#include "Logger.hpp"

#define MATERIAL_LOAD_DEBUG 0

bool import_utils::mtl_importer::import_mtl(MeshManager & mesh, std::ifstream & file) {
	std::string line;
	int currentIndex = 0;
	std::map<texture_manager**, std::string> load_map;
	std::string mat_name;
	while (std::getline(file, line)) {
		material_manager * current_material = material_manager::get_resource(mat_name, false);

#if MATERIAL_LOAD_DEBUG
		std::cout << line << std::endl;
#endif
		std::string header;
		std::istringstream lineStream(line);
		//Read in the header
		lineStream >> header;

		if (header.empty()) continue;

		//Line is a vertex line
		if (header == "newmtl") {
			lineStream >> mat_name;
#if MATERIAL_LOAD_DEBUG
			std::cout << "Loaded material "<< mat_name << std::endl;
#endif
		}
		else if (header == "illum") {
			lineStream >> current_material->illum;
		}
			//Channel defs
		else if (header == "Ka") {
			glm::vec3 color;
			lineStream >> current_material->amb_color;
		}
		else if (header == "map_Ka") {
			std::string name;
			lineStream >> name;
			load_map[&current_material->amb_map] = name;
		}
		else if (header == "Kd") {
			glm::vec3 color;
			lineStream >> current_material->diffus_color;
		}
		else if (header == "map_Kd") {
			std::string name;
			lineStream >> name;

			load_map[&current_material->diffus_map] = name;
		}
		else if (header == "Ks") {
			glm::vec3 color;
			lineStream >> current_material->spec_color;
		}
		else if (header == "map_Ks") {
			std::string name;
			lineStream >> name;
			load_map[&current_material->spec_map] = name;
		}
		else if (header == "Ke") {
			glm::vec3 color;
			lineStream >> current_material->emissive_color;
		}
		else if (header == "map_Ke") {
			std::string name;
			lineStream >> name;
			load_map[&current_material->emissive_map] = name;
		}
		else if (header == "Ni") {
			glm::vec3 color;
			lineStream >> current_material->anisotropy;
		}
		else if (header == "map_Ni") {
			std::string name;
			lineStream >> name;
			load_map[&current_material->anisotropy_map] = name;
		}
			//Specular exponent
		else if (header == "Ns") {
			float exponent;
			lineStream >> current_material->spec_exponent;
		}
			//opacity
		else if (header == "d") {
			float transp;
			lineStream >> transp;
			current_material->opacity = transp;
			if (approx(current_material->opacity, 0.25f))
				current_material->clip = true;
		}
			//inverted opacity
		else if (header == "Tr") {
			float transp;
			lineStream >> transp;
			current_material->opacity = transp;
			if (approx(current_material->opacity, 0.25f))
				current_material->clip = true;
		}
		else if (header == "#") {
			//Comment, nothin to do
		}
		else {
			LOG(fmt::format("Unable to parse line \"{}\" in mtl file for mesh {}", line, mesh.name), LogType::Error);
		}
	}
	using namespace boost::asio::detail;


	std::vector<texture_manager**> key_vec;
	std::vector<std::string> val_vec;
	for (auto & pair : load_map) {
		key_vec.push_back(pair.first);
		val_vec.push_back(pair.second);
	}


	//thread_group group;
	for (int i = 0; i < key_vec.size(); i++) {
		//texture_manager ** manager = key_vec[i];
		//std::string name = val_vec[i];
		*key_vec[i] = texture_manager::get_resource(val_vec[i], false);
		//group.create_thread([manager, name, i]() { *manager = texture_manager::get_resource(name, true); });
	}
	LOG(fmt::format("Loaded {} textures for materials used on mesh {}", key_vec.size(), mesh.name), LogType::Comment);
	//group.join();
	//


	for (auto & pair : load_map)
		(*pair.first)->init_image();

	return true;
}

import_utils::mtl_importer::mtl_importer() {
}


import_utils::mtl_importer::~mtl_importer() {
}
