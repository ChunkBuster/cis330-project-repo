#pragma once
#include "ShaderManager.hpp"
#include "MeshManager.hpp"

class transform;

class renderer {
public:
	renderer(GLuint VAO, GLuint VBO, GLuint UVBO, GLuint NBO, GLuint EBO, ShaderManager * shader,
	         std::vector<MeshManager::sub_mesh> indexBufferVec);
	~renderer();
	void render();

	transform * transf;
	void render_sub_mesh(MeshManager::sub_mesh sub) const;

private:
	//Binding point for submesh indices
	std::vector<MeshManager::sub_mesh> opaque_sub_meshes;
	std::vector<MeshManager::sub_mesh> transp_sub_meshes;
	/*GLuint FramebufferName;
	GLuint QuadProgramID;
	GLuint DepthMatrixID;
	GLuint DepthProgramID;*/
	//shader_manager *shaderManager = shader_manager::getShader("SimpleShader");
	//Vertex Array Object. All the following Attributes are stored in this, so that they do not have to be re-
	//bumped to the GPU each and every frame for each and every vertex. This saves a shitload of bandwidth.
	GLuint VAO;
	//VBO: Vertex Buffer Object. Used to send multiple verts to the GPU at the same time, so that they may be 
	//accessed quickly.
	GLuint VBO;
	//Binding point for UV buffer
	GLuint UVBO;
	//Binding point for normal buffer
	GLuint NBO;
	//Index buffer for entire object
	GLuint EBO;

	ShaderManager * shader;
};
