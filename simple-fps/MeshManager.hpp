#pragma once
#include "ResourceManager.hpp"
#include "ShaderManager.hpp"
#include "Defines.hpp"
#include "MaterialManager.hpp"
class renderer;

class MeshManager : public ResourceManager<MeshManager> {
public:
	struct sub_mesh {
		idxvec indices;
		GLuint EBO;
		material_manager * mat;
		bool smooth;

		sub_mesh(idxvec ind, material_manager * m) : EBO(0) {
			indices = ind;
			mat = m;
		}
	};

	MeshManager(std::string name);
	~MeshManager();

	void bindBuffers();

	static void pushIndexValue(int val, idxvec & vector);

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	idxvec indices;
	//Binding point for submesh indices
	std::vector<sub_mesh> subMeshes;

	unsigned int mesh;

	std::string dir() override;

	renderer* get_renderer() const;
protected:
	bool loadResource(std::string name, bool mt) override;

private:
	GLuint FramebufferName;
	GLuint QuadProgramID;
	GLuint DepthMatrixID;
	GLuint DepthProgramID;
	//shader_manager *shaderManager = shader_manager::getShader("SimpleShader");
	//Vertex Array Object. All the following Attributes are stored in this, so that they do not have to be re-
	//bumped to the GPU each and every frame for each and every vertex. This saves a shitload of bandwidth.
	GLuint VAO;
	//VBO: Vertex Buffer Object. Used to send multiple verts to the GPU at the same time, so that they may be 
	//accessed quickly.
	GLuint VBO;
	//Binding point for UV buffer
	GLuint UVBO;
	//Binding point for normal buffer
	GLuint NBO;
	//Index buffer for entire object
	GLuint EBO;

	ShaderManager * shader;
};
