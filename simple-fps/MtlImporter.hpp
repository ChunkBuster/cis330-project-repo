#pragma once
#include "MaterialManager.hpp"

class MeshManager;
class texture_manager;

namespace import_utils
{
	class mtl_importer {
	public:
		mtl_importer();
		static bool import_mtl(MeshManager & manager, std::ifstream & file);
		~mtl_importer();
	};
}
