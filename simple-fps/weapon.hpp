#pragma once
#include "fwd.h"
#include "Transform.hpp"

class Weapon {
public:
	Weapon();
	~Weapon();
	void poll(int key, int action);
	renderer * rend;

	struct weapon_properties {
		float fire_rate = 0.1f;
		float brightness = 2.0f;
		glm::vec3 color = {1, 0.4f, 0.1};
		float kickback = 0.03f;
		glm::vec3 start_pos;

		weapon_properties(glm::vec3 startPos) : start_pos(startPos) {
		}
	};

	weapon_properties properties;
private:

	bool firing;
	transform tip;
	void update_gun_values() const;
	float brightness = 0;
};
