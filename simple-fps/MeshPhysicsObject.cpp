#include "stdafx.h"
#include "MeshPhysicsObject.hpp"
#include "PhysWorld.hpp"
#include "MeshManager.hpp"

MeshPhysicsObject::MeshPhysicsObject(MeshManager *model, bool isStatic) : PhysicsObject(isStatic) {
	this->model = model;
	if (isStatic)
		load_static_collision_shape(model);
	else
		load_dynamic_collision_shape(model);
}


MeshPhysicsObject::~MeshPhysicsObject() {
}


void MeshPhysicsObject::load_static_collision_shape(const MeshManager *model) {
	dynamic = false;
	unoptimized_hull = nullptr;
	hull_optimizer = nullptr;
	hull = nullptr;

	triangle_mesh = new btTriangleMesh{};
	for (unsigned i{0}; i < model->indices.size(); i += 3) {
		btVector3 vertex_1{
			model->vertices[model->indices[i]].x,
			model->vertices[model->indices[i]].y,
			model->vertices[model->indices[i]].z
		};
		btVector3 vertex_2{
			model->vertices[model->indices[i + 1]].x,
			model->vertices[model->indices[i + 1]].y,
			model->vertices[model->indices[i + 1]].z
		};
		btVector3 vertex_3{
			model->vertices[model->indices[i + 2]].x,
			model->vertices[model->indices[i + 2]].y,
			model->vertices[model->indices[i + 2]].z
		};
		triangle_mesh->addTriangle(vertex_1, vertex_2, vertex_3);
	}
	triangle_mesh_shape = new btBvhTriangleMeshShape{triangle_mesh, true};

	collision_shape = new btBvhTriangleMeshShape{*triangle_mesh_shape};

	motion_state =
		new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 0, 0)));
	btRigidBody::btRigidBodyConstructionInfo groundRigidBodyCI(0, motion_state, collision_shape, btVector3(0, 0, 0));
	rb = new btRigidBody(groundRigidBodyCI);
	PhysWorld::register_rigidbody(rb);
}

void MeshPhysicsObject::load_dynamic_collision_shape(const MeshManager * model) {
	dynamic = true;
	triangle_mesh = nullptr;
	triangle_mesh_shape = nullptr;

	unoptimized_hull = new btConvexHullShape{};
	for (GLuint i : model->indices) {
		btVector3 vertex{
			model->vertices[i].x,
			model->vertices[i].y, model->vertices[i].z
		};
		unoptimized_hull->addPoint(vertex);
	}

	hull_optimizer = new btShapeHull{unoptimized_hull};
	btScalar margin{unoptimized_hull->getMargin()};
	hull_optimizer->buildHull(margin);
	hull = new btConvexHullShape{
		(btScalar*)hull_optimizer->getVertexPointer(),
		hull_optimizer->numVertices()
	};

	collision_shape = new btConvexHullShape{*hull};

	//Init ball
	motion_state = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 0, 0)));
	//Give ball physical properties, like mass
	btScalar mass = 1;
	btVector3 fallInertia(0, 0, 0);
	collision_shape->calculateLocalInertia(mass, fallInertia);
	const btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(mass, motion_state, collision_shape, fallInertia);
	rb = new btRigidBody(fallRigidBodyCI);
	PhysWorld::register_rigidbody(rb);
}
