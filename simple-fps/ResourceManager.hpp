#pragma once
#include <map>
#include <regex>
#include "Logger.hpp"

template <class ResourceType>
class ResourceManager {
public:
	explicit ResourceManager(std::string name);
	virtual ~ResourceManager();

	static ResourceType* get_resource(std::string name, bool mt);
	static ResourceType* touch_resource(std::string name);

	typedef std::map<std::string, ResourceType*> resource_map;
	std::string name;
	virtual std::string dir() = 0;

protected:
	virtual bool loadResource(std::string name, bool mt = false) = 0;
	bool loaded = false;

private:
	static resource_map _resourceMap;
};


template <class ResourceType>
typename ResourceManager<ResourceType>::resource_map ResourceManager<ResourceType>::_resourceMap = {};


template <class ResourceType>
std::string ResourceManager<ResourceType>::dir() {
	return std::string("resources/");
}

template <class ResourceType>
ResourceManager<ResourceType>::ResourceManager(std::string name) {
	this->name = name;
	_resourceMap[name] = static_cast<ResourceType*>(this);
}

template <class ResourceType>
ResourceManager<ResourceType>::~ResourceManager() {

}

template <class ResourceType>
ResourceType* ResourceManager<ResourceType>::touch_resource(std::string name) {
	name = regex_replace(name, std::regex(R"(\\\\)"), "/");
	name = regex_replace(name, std::regex(R"(\\)"), "/");
	ResourceManager<ResourceType> * resource = _resourceMap[name];
	if (!resource || resource->loaded)
		resource = new ResourceType(name);
	else
		resource->loaded = true;

	return static_cast<ResourceType*>(resource);
}

template <class ResourceType>
ResourceType* ResourceManager<ResourceType>::get_resource(std::string name, bool mt = false) {
	name = regex_replace(name, std::regex(R"(\\\\)"), "/");
	name = regex_replace(name, std::regex(R"(\\)"), "/");
	ResourceManager<ResourceType> * resource = _resourceMap[name];
	//if(!resource)
	//	LOG(fmt::format("Resource {} not found, loading", name), LogType::Message);
	if (!resource || !resource->loaded) {
		resource = new ResourceType(name);
		if (!resource->loadResource(name, mt))
			LOG(fmt::format("Failed to load resource {}", name), LogType::Error);
		else
			resource->loaded = true;
	}

	return static_cast<ResourceType*>(resource);
}
