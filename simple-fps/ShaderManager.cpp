﻿#include "stdafx.h"
#include "ShaderManager.hpp"
#include "TextureManager.hpp"

ShaderManager::ShaderManager(std::string name) :
	ResourceManager<ShaderManager>(name),
	shaderProgram(glCreateProgram()),
	uniforms() {

}

GLint ShaderManager::compileShader(unsigned int vertexShader, unsigned int fragmentShader, char infoLog[]) const {
	GLint statusCode;
	//Attach the shaders to the program
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	//This calls OpenGL's internal linker to link together the vertex and fragment shaders. 
	glLinkProgram(shaderProgram);

	//This too may fail, so we must check for errors as above
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &statusCode);
	if (!statusCode) {
		glGetProgramInfoLog(shaderProgram, 512, nullptr, infoLog);
		LOG(fmt::format("ERROR::LINKER::LINKING_FAILED {} ", name, std::string(infoLog)), LogType::Message);
	}

	//The constituents are no longer needed; dispose of them.
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	return statusCode;
}

bool ShaderManager::loadResource(std::string name, bool mt) {
	std::string vertLocation = dir() + name + ".vertexshader";
	std::string fragLocation = dir() + name + ".fragmentshader";
	//We need to compile the shader at runtime for OpenGL to use it. The uint is the ID, as before.
	unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);

	//Create the file stream
	std::ifstream inFile;
	inFile.open(vertLocation);

	if (!inFile) {
		LOG(fmt::format("Unable to open file {} for vertex shader {}", vertLocation, name), LogType::Error);
		return false;
	}
	std::stringstream buffer;
	//Read the contents of the file
	buffer << inFile.rdbuf();
	//Convert to string 
	std::string vertexString = buffer.str();
	const char * vertexShaderSource = vertexString.c_str();
	LOG(fmt::format("Loaded files for shader {}", name), LogType::Comment);
	//Assign source, number of strings to use to compile, then pull the trigger on compilation
	glShaderSource(vertexShader, 1, &vertexShaderSource, nullptr);
	glCompileShader(vertexShader);
	LOG(fmt::format("Compiled vertex shader for {}", name), LogType::Special, u8"◇");
	//After the shader is compiled, checking if it actually functioned would be prudent.
	int success;
	//Buffer to log errors into
	char infoLog[512];
	//Check to make sure that the shader compiled. If it did not, write an error code
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		//If the check failed, write the error log to the infolog buffer and then output the error.
		glGetShaderInfoLog(vertexShader, 512, nullptr, infoLog);
		LOG(fmt::format("ERROR::SHADER::VERTEX::COMPILATION_FAILED {} for vertex shader {} : {}", vertLocation, name, std::
			string(infoLog)), LogType::Error);
		return false;
	}

	std::ifstream inFileFrag;
	inFileFrag.open(fragLocation);

	if (!inFileFrag) {
		LOG(fmt::format("Unable to open file {} for fragment shader {}", fragLocation, name, std::string(infoLog)), LogType::
			Error);
		return false;
	}

	std::stringstream bufferFrag;
	//Read the contents of the file
	bufferFrag << inFileFrag.rdbuf();
	//Convert to string 
	std::string fragmentString = bufferFrag.str();
	const char * fragmentShaderSource = fragmentString.c_str();

	// fragment shader
	unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, nullptr);
	glCompileShader(fragmentShader);
	// check for shader compile errors
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(fragmentShader, 512, nullptr, infoLog);
		LOG(fmt::format("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED : {}", std::string(infoLog)), LogType::Error);
		return false;
	}
	LOG(fmt::format("Compiled fragment shader for {}", name), LogType::Special, u8"◆");
	//Now that the vertex and fragment shaders have been successfully compiled, it's time to turn them into one object
	if (compileShader(vertexShader, fragmentShader, infoLog) == GL_TRUE) {
		LOG(fmt::format("Successfully linked shader {}", name), LogType::Special, u8"◈");
	}

	return true;
}

std::string ShaderManager::dir() {
	return ResourceManager<ShaderManager>::dir() + "shaders/";
}

ShaderManager::~ShaderManager() {
}

GLuint ShaderManager::get_uniform(std::string uniform_name) {
	GLuint uniformLocation = uniforms[uniform_name];
	if (!uniformLocation) {
		uniformLocation = glGetUniformLocation(shaderProgram, uniform_name.c_str());
		if (uniformLocation == GL_INVALID_VALUE)
			LOG(fmt::format("Attempted to get uniform \"{}\" from shader {} , but it was not found in the program", uniform_name,
			name), LogType::Warning);
		uniforms[name] = uniformLocation;
	}
	return uniformLocation;
}
