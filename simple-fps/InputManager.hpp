#pragma once
#include "fwd.h"

class Weapon;
class transform;
class renderer;

class InputManager {

public:
	static ImGuiIO * io;

	static glm::mat4 viewMatrix;
	static glm::vec3 position;
	static glm::vec2 sensitivity;
	static glm::vec2 moveSpeed;

	static transform player_transform;
	static double xRot;
	static double yRot;
	static glm::vec2 screenSize;
	static glm::vec2 screenMiddle;

	static double lastTime;
	static double delta_time;

	static Weapon * player_gun;

	static void setCursorLock(GLFWwindow * window, bool lock);

	static void init_ui(GLFWwindow * window);
	static void process_input(GLFWwindow * window);
	static void draw_imgui();

	static boost::asio::io_service ioService;

private:

};

void framebuffer_size_callback(GLFWwindow * window, int width, int height);
