#if SPLIT_STREAM
#pragma once
struct stream_override : public std::streambuf
{
	std::stringstream *of_stream_split;
	std::ostream *backup_cout;

	void test()
	{
		int ass = 3;
	}

	template<typename T>
	stream_override& operator<<(const T& val)
	{
		*backup_cout << val;
		*of_stream_split << val;
		test();
		return *this;
	}

	typedef std::ostream& (*stream_function)(std::ostream&);
	stream_override& operator<<(stream_function func)
	{
		func(*backup_cout);
		func(*of_stream_split);
		test();
		return *this;
	}

protected:
	std::streamsize xsputn(const char_type* s, std::streamsize n) override;
	int_type overflow(int_type ch) override;
};
#endif
