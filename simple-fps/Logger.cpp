﻿#include "stdafx.h"
#include "Logger.hpp"
#include "imgui_console.hpp"

Logger * Logger::_Instance = new Logger();

Logger::Logger() : _console(new user_input::ExampleAppConsole()) {

}


Logger::~Logger() {
}

void Logger::log_imp(std::string message, LogType logType, int line, const char * func, const char * file) {
	std::string prefix;
	switch (logType) {
	case(Comment):
		prefix = LOG_COMMENT_SYMBOL;
		break;
	case(Message):
		prefix = LOG_MESSAGE_SYMBOL;
		break;
	case(Warning):
		prefix = LOG_WARNING_SYMBOL;
		break;
	default:
	case(Error):
		prefix = LOG_ERROR_SYMBOL;
		break;
	}
	log_imp(message, logType, prefix, line, func, file);
}


void Logger::log_imp(std::string message, LogType logType, std::string glyph, int line, const char * func,
                     const char * file) {
	switch (logType) {
	case(Error):
		_Instance->_errors++;
		break;
	case(Warning):
		_Instance->_warnings++;
		break;
	default:
	case(Message):
		_Instance->_messages++;
		break;

	}
	std::string & usable_prefix = glyph;
	std::string suffix;
	if (logType == Error)
		suffix = fmt::format(u8"│file \"{}\"│func \"{}\"│line {}", file, func, line);
	std::string formatted = fmt::format(u8"{}{}│{}{}", logType, usable_prefix, message, suffix);
	const char * message_cstr = formatted.c_str();
	_Instance->_console->AddLog(message_cstr);
}

void Logger::set_log_level(int level) {
	_Instance->_logLevel = level;
}

void Logger::draw_console() {
	std::stringstream title;
	title << u8"Console │" << _Instance->_messages << u8" Messages │" << _Instance->_warnings << u8" Warnings│" <<
		_Instance->_errors << u8" Errors";
	const std::string & tmp = title.str();
	const char * title_str(tmp.c_str());
	_Instance->_console->Draw(title_str);
}
