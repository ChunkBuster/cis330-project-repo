#pragma once
#include "Defines.hpp"
#include <map>

namespace import_utils
{
	inline bool approx(float x, float y) {
		return fabs(x - y) < 0.01f;
	}

	struct MaterialAssocData {
		std::vector<glm::vec3> positions;
		std::vector<glm::vec2> uvs;
		std::vector<glm::vec3> normals;
	};

	struct IndexGroup {
		idxvec vertIndices;
		idxvec uvIndices;
		idxvec normalIndices;
		idxvec initialIndices;
		idxvec finalIndices;

		IndexGroup() {
		}

		IndexGroup(idxvec & vertIndices, idxvec & uvIndices, idxvec & normalIndices)
			: vertIndices(vertIndices), uvIndices(uvIndices), normalIndices(normalIndices) {
		}
	};

	struct KeyFuncs {
		size_t operator()(const glm::vec3 & k) const {
			return std::hash<int>()(k.x) ^ std::hash<int>()(k.y);
		}

		bool operator()(const glm::vec3 & a, const glm::vec3 & b) const {
			return approx(a.x, b.x) && approx(a.y, b.y) && approx(a.z, b.z);
		}

		size_t operator()(const glm::vec2 & k) const {
			return std::hash<int>()(k.x) ^ std::hash<int>()(k.y);
		}

		bool operator()(const glm::vec2 & a, const glm::vec2 & b) const {
			return approx(a.x, b.x) && approx(a.y, b.y);
		}
	};


	typedef std::map<glm::vec3, glm::vec3, KeyFuncs, KeyFuncs> vec3Map;
	typedef std::map<glm::vec2, glm::vec2, KeyFuncs, KeyFuncs> vec2Map;

	struct PackedVertex {
		glm::vec3 position;
		glm::vec2 uv;
		glm::vec3 normal;

		bool operator<(const PackedVertex that) const {
			return memcmp((void*)this, (void*)&that, sizeof(PackedVertex)) > 0;
		};
	};

	inline bool getSimilarVertexIndex_fast(
		PackedVertex & packed,
		std::map<PackedVertex, idxVal> & VertexToOutIndex,
		idxVal & result
	) {
		std::map<PackedVertex, idxVal>::iterator it = VertexToOutIndex.find(packed);
		if (it == VertexToOutIndex.end()) {
			return false;
		}
		result = it->second;
		return true;
	}

	inline void indexVBO(
		std::vector<glm::vec3> & in_vertices,
		std::vector<glm::vec2> & in_uvs,
		std::vector<glm::vec3> & in_normals,

		idxvec & out_indices,

		std::vector<glm::vec3> & out_vertices,
		std::vector<glm::vec2> & out_uvs,
		std::vector<glm::vec3> & out_normals,

		std::map<PackedVertex, idxVal> & VertexToOutIndex
	) {
		for (unsigned int i = 0; i < in_vertices.size(); i++) {
			PackedVertex packed = {in_vertices[i], in_uvs[i], in_normals[i]};
			idxVal index;
			if (!getSimilarVertexIndex_fast(packed, VertexToOutIndex, index)) {
				out_vertices.push_back(in_vertices[i]);
				out_uvs.push_back(in_uvs[i]);
				out_normals.push_back(in_normals[i]);
				//Assign the new index
				index = static_cast<idxVal>(out_vertices.size()) - 1;
				VertexToOutIndex[packed] = index;
			}
			out_indices.push_back(index);
		}
	}
}
