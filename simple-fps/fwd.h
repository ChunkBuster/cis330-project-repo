#pragma once
#include <glm/fwd.hpp>

class texture_manager;
class ShaderManager;
class MeshManager;
class transform;
class Weapon;
class renderer;
//Bullet
class btConvexHullShape;
class btShapeHull;
class btTriangleMesh;
class btBvhTriangleMeshShape;
class btCollisionShape;
class btDefaultMotionState;
class btRigidBody;
class btBroadphaseInterface;
class btCollisionDispatcher;
class btDefaultCollisionConfiguration;
class btSequentialImpulseConstraintSolver;
class btDiscreteDynamicsWorld;
