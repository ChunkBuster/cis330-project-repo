#pragma once
class MeshManager;

namespace import_utils
{
	class obj_importer {
	public:
		obj_importer();
		static bool import_obj(MeshManager & manager, std::ifstream & file);
		~obj_importer();
	};
}
