#include "stdafx.h"
#include "Defines.hpp"
#include "ObjImporter.hpp"
#include "MeshManager.hpp"

#include "MeshImportUtils.hpp"
#include "MtlImporter.hpp"
#include "Logger.hpp"
#include <boost/algorithm/string.hpp>

import_utils::obj_importer::obj_importer() {
}

bool import_utils::obj_importer::import_obj(MeshManager & mesh, std::ifstream & file) {
	idxvec vertexIndices, uvIndices, normalIndices;
	std::vector<glm::vec3> temp_vertices;
	std::vector<glm::vec2> temp_uvs;
	std::vector<glm::vec3> temp_normals;

	//Current material
	std::string mtllib;
	std::string material;

	std::map<std::string, IndexGroup> matMap;
	bool smooth = false;
	std::string group;
	std::string object;
	//Load and parse the file
	std::string line;

	int currentIndex = 0;
	while (std::getline(file, line)) {
		if (matMap.find(material) == matMap.end()) {
			const IndexGroup indexGroup;
			matMap[material] = indexGroup;
		}

		IndexGroup * idex = &matMap[material];

#ifdef MESH_LOAD_DEBUG
		std::cout << line << std::endl;
#endif

		std::string header;
		std::istringstream lineStream(line);
		//Read in the header
		lineStream >> header;
		if (header.empty()) continue;

		//Line is a vertex line
		if (header == "v") {
			glm::vec3 vertex;
			lineStream >> vertex;
			temp_vertices.push_back(vertex);
			//idex->verts->push_back(vertex);
#ifdef MESH_LOAD_DEBUG
			std::cout << '(' << vertex.x << ',' << vertex.y << ',' << vertex.z << ')' << std::endl;
#endif
		}
			//Line is a UV line
		else if (header == "vt") {
			glm::vec2 uv;
			lineStream >> uv;
			temp_uvs.push_back(uv);
			//idex->uvs->push_back(uv);
#ifdef MESH_LOAD_DEBUG
			std::cout << '(' << uv.x << ',' << uv.y << ')' << std::endl;
#endif
		}
			//Line is a normal line
		else if (header == "vn") {
			glm::vec3 normal;
			lineStream >> normal;
			temp_normals.push_back(normal);
			//idex->normals->push_back(normal);
#ifdef MESH_LOAD_DEBUG
			std::cout << '(' << normal.x << ',' << normal.y << ',' << normal.z << ')' << std::endl;
#endif
		}
			//Line is a material data line
		else if (header == "usemtl") {
			lineStream >> material;
			material = material;
		}
			//Line is a face line
		else if (header == "f") {
			std::string tripletString;
			while (lineStream >> tripletString) {
				if (tripletString.empty()) continue;

				std::istringstream tripletStream(tripletString);
				std::string token;
				std::vector<int> indexData;

				while (std::getline(tripletStream, token, '/')) {
					if (token.empty()) {
						indexData.push_back(-1);
						continue;
					}
					const int val = stoi(token);
					indexData.push_back(val);

				}
#ifdef MESH_LOAD_DEBUG
				std::cout << ' ';
#endif
				//Push values to vectors
				mesh.pushIndexValue(indexData[0], vertexIndices);
				mesh.pushIndexValue(indexData[1], uvIndices);
				mesh.pushIndexValue(indexData[2], normalIndices);

				//Currently, the index is simply the 
				mesh.pushIndexValue(indexData[0], idex->vertIndices);
				mesh.pushIndexValue(indexData[1], idex->uvIndices);
				mesh.pushIndexValue(indexData[2], idex->normalIndices);

				int curInitIdex = currentIndex;
				idex->initialIndices.push_back(currentIndex++);
				matMap[material] = *idex;

				for (auto i = 0; i < indexData.size(); i++) {
					if (indexData[i] > USHRT_MAX || indexData[i] < 0) {
						LOG(fmt::format("Model contained invalid index {}!", indexData[i]), LogType::Error);
					}
				}
			}

#ifdef MESH_LOAD_DEBUG
			std::cout << vertexIndices[vertexIndices.size() - 3] << "/" << vertexIndices[vertexIndices.size() - 2] << "/" << vertexIndices[vertexIndices.size() - 1];
			std::cout << '\n';
#endif

		}
		else if (header == "g") {
			lineStream >> group;
			//Do something with this later
		}
		else if (header == "s") {
			std::string flag;
			lineStream >> flag;
			smooth = flag != "off";
			//Do something with this later
		}
		else if (header == "o") {
			std::string name;
			lineStream >> name;
			object = name;
			//Do something with this later
		}
		else if (header == "mtllib") {
			lineStream >> mtllib;
		}
			//Line is a comment line
		else if (header == "#") {
			//Comment, nothin to do
		}
		else {
			LOG(fmt::format("Unable to parse line \"{}\" in obj file for mesh {}", line, mesh.name), LogType::Error);
		}
	}

	//Process the parsed data, pushing to buffers that are consumed later. Duplicate verts not yet checked.
	for (unsigned int i = 0; i < vertexIndices.size(); i++) {
		idxVal vertexIndex = vertexIndices[i];
		//off-by-one offset due to obj indexing being weird
		glm::vec3 vertex = temp_vertices[vertexIndex - 1];
		mesh.vertices.push_back(vertex);
	}

	for (unsigned int i = 0; i < uvIndices.size(); i++) {
		idxVal uvIndex = uvIndices[i];
		glm::vec2 uv = temp_uvs[uvIndex - 1];
		mesh.uvs.push_back(uv);
	}

	for (unsigned int i = 0; i < normalIndices.size(); i++) {
		idxVal normalIndex = normalIndices[i];
		glm::vec3 norm = temp_normals[normalIndex - 1];
		mesh.normals.push_back(norm);
	}

	std::vector<MaterialAssocData> materialData;

	auto old_vertices = mesh.vertices;
	auto old_uvs = mesh.uvs;
	auto old_normals = mesh.normals;

	std::vector<glm::vec3> out_vertices;
	std::vector<glm::vec2> out_uvs;
	std::vector<glm::vec3> out_normals;

	idxvec out_indices;

	std::map<PackedVertex, idxVal> out_combo_index_assoc;

	indexVBO(mesh.vertices, mesh.uvs, mesh.normals, out_indices, out_vertices, out_uvs, out_normals,
	         out_combo_index_assoc);

	mesh.vertices = out_vertices;
	mesh.uvs = out_uvs;
	mesh.normals = out_normals;
	mesh.indices = out_indices;

	auto indexMap = out_combo_index_assoc;

	std::map<PackedVertex, idxVal> out_combo_index_assoc_2;

	for (const auto & pair : matMap) {
		IndexGroup * idex = &matMap[pair.first];
		for (unsigned int i = 0; i < idex->initialIndices.size(); i++) {
			PackedVertex packed = {
				old_vertices[idex->initialIndices[i]],
				old_uvs[idex->initialIndices[i]],
				old_normals[idex->initialIndices[i]]
			};

			idxVal index;
			if (!getSimilarVertexIndex_fast(packed, indexMap, index))
				LOG(fmt::format("Could not find pre-indexed vert on mesh {}", mesh.name), LogType::Error);

			idex->finalIndices.push_back(index);
		}
	}

	std::string location = mesh.dir() + mtllib;
	std::ifstream mtlfile;
	mtlfile.open(location, std::ifstream::in);
	//FILE * file = fopen(c_str(location, "r");
	if (!mtlfile) {
		LOG(fmt::format("Unable to open mtllib {} for obj file {}", location, mesh.name), LogType::Error);
		return false;
	}

	if (mtl_importer::import_mtl(mesh, mtlfile)) {
		//Assign final index values to all submeshes
		for (const auto & pair : matMap) {
			if (pair.second.finalIndices.size() <= 0)
				continue;
			// texture_manager::getResource(mesh.Name + "_textures/" + pair.first + ".jpg")

			mesh.subMeshes.push_back(
				MeshManager::sub_mesh(pair.second.finalIndices, material_manager::get_resource(pair.first, false)));
		}
	}
	mtlfile.close();
	return true;
}


import_utils::obj_importer::~obj_importer() {
}
