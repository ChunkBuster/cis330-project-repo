#pragma once
#include "fwd.h"
#include "ResourceManager.hpp"

class material_manager : public ResourceManager<material_manager> {
public:
	material_manager(std::string name);
	~material_manager();

	/*0. Color on and Ambient off
	1. Color on and Ambient on
	2. Highlight on
	3. Reflection on and Ray trace on
	4. Transparency: Glass on, Reflection: Ray trace on
	5. Reflection: Fresnel on and Ray trace on
	6. Transparency: Refraction on, Reflection: Fresnel off and Ray trace on
	7. Transparency: Refraction on, Reflection: Fresnel on and Ray trace on
	8. Reflection on and Ray trace off
	9. Transparency: Glass on, Reflection: Ray trace off
	10. Casts shadows onto invisible surfaces*/
	int illum = 0;
	bool clip = false;

	glm::vec3 amb_color = glm::vec3(0);
	glm::vec3 diffus_color = glm::vec3(1);
	glm::vec3 spec_color = glm::vec3(0);
	glm::vec3 emissive_color = glm::vec3(0);

	float spec_exponent = 1;
	float opacity = 1;
	float anisotropy = 0;

	texture_manager * amb_map;
	texture_manager * diffus_map;
	texture_manager * emissive_map;
	texture_manager * spec_map;
	texture_manager * normal_map;
	texture_manager * bump_map;
	texture_manager * stencil_map;
	texture_manager * anisotropy_map;

	std::string dir() override;
protected:
	bool loadResource(std::string name, bool mt) override;
};
