#include "stdafx.h"
#include "Defines.hpp"
#include "ShaderManager.hpp"
#include "TextureManager.hpp"
#include "MeshManager.hpp"
#include "ObjImporter.hpp"
#include "Renderer.hpp"
#include "Logger.hpp"
//#include <iso646.h>

#define SHADOWMAP_SIZE 1024


//#define MESH_LOAD_DEBUG
MeshManager::MeshManager(std::string name) : ResourceManager(name) {
	shader = ShaderManager::get_resource("StandardShading", false);
}

MeshManager::~MeshManager() {
	for (auto & m : subMeshes) {
		/*
		delete m.indices;
		delete m.EBO;
		delete m.mat;*/
	}
}

void MeshManager::bindBuffers() {
	//Shadowmap shit
	// The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
	GLuint FramebufferName = 0;
	glGenFramebuffers(1, &FramebufferName);
#if LIGHT_MAP
	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);

	// Depth texture. Slower than a depth buffer, but you can sample it later in your shader
	GLuint depthTexture;
	glGenTextures(1, &depthTexture);
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, SHADOWMAP_SIZE, SHADOWMAP_SIZE, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture, 0);

	// No color output in the bound framebuffer, only depth.
	glDrawBuffer(GL_NONE);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cerr << "Could not load frame buffer!" << std::endl;


	// The quad's FBO. Used only for visualizing the shadowmap.
	static const GLfloat g_quad_vertex_buffer_data[] = {
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		-1.0f,  1.0f, 0.0f,
		-1.0f,  1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		1.0f,  1.0f, 0.0f,
	};

	GLuint quad_vertexbuffer;
	glGenBuffers(1, &quad_vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, quad_vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_quad_vertex_buffer_data), g_quad_vertex_buffer_data, GL_STATIC_DRAW);

	// Create and compile our GLSL program from the shaders
	QuadProgramID = shader_manager::getResource("Passthrough")->shaderProgram;
	//ShadowTexID = glGetUniformLocation(QuadProgramID, "texture");

	//Load shader used to capture depth map for lighting purposes
	DepthProgramID = shader_manager::getResource("DepthRTT")->shaderProgram;

	DepthMatrixID = glGetUniformLocation(DepthProgramID, "depthMVP");
#endif
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	//We obtain the pointer to this uint, and then tell it to generate one single ID for all of the verts.
	//The value is assigned back to the uint.
	glGenBuffers(1, &VBO);
	//Bind the buffer to the ID. From this point on, any calls that modify the GL_ARRAY_BUFFER will directly
	//modify the actual contents on the gpu.
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	if (vertices.size() > 0)
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
	else
		LOG(fmt::format("No Verts found for mesh {}", name), LogType::Error);

	glGenBuffers(1, &UVBO);
	glBindBuffer(GL_ARRAY_BUFFER, UVBO);
	if (uvs.size() > 0)
		glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
	else
		LOG(fmt::format("No UVs found for mesh {}", name), LogType::Warning);

	glGenBuffers(1, &NBO);
	glBindBuffer(GL_ARRAY_BUFFER, NBO);
	if (normals.size() > 0)
		glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);
	else
		LOG(fmt::format("No Normals found for mesh {}", name), LogType::Warning);

	//General ebo
	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	if (indices.size() > 0)
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(idxVal), &indices[0], GL_STATIC_DRAW);
	else
		LOG(fmt::format("No indices found for mesh {}", name), LogType::Error);

	//submesh ebos
	for (auto & sub : subMeshes) {
		glGenBuffers(1, &sub.EBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sub.EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sub.indices.size() * sizeof(idxVal), &sub.indices[0], GL_STATIC_DRAW);
	}
}

void MeshManager::pushIndexValue(int val, idxvec & vector) {
	if (val != -1)
		vector.push_back(val);
}

std::string MeshManager::dir() {
	return ResourceManager<MeshManager>::dir() + "Meshes/";
}

renderer* MeshManager::get_renderer() const {
	return new renderer(VAO, VBO, UVBO, NBO, EBO, shader, subMeshes);
}

bool MeshManager::loadResource(std::string name, bool mt) {
	std::string location = dir() + name + ".obj";
	std::ifstream file;
	file.open(location, std::ifstream::in);
	if (!file) {
		LOG(fmt::format("Unable to open {}", location), LogType::Error);
		return false;
	}

	std::string extension = location.substr(location.find_last_of(".") + 1);

	if (extension == "obj") {
		if (!import_utils::obj_importer::import_obj(*this, file))
			return false;
	}
	else {
		LOG(fmt::format("Unknown file type for mesh {} : \"{}\"", extension), LogType::Error);
		return false;
	}

	file.close();

	bindBuffers();
#if VERBOSE_RESOURCES
	LOG(fmt::format("Loaded mesh \"{}\" from file {}", name, location), LogType::Message);
	LOG(fmt::format("Contained {} submeshes: ", subMeshes.size()), LogType::Comment);
	int submeshCounter = 0;
	for (const auto & sub : subMeshes) {
		LOG(fmt::format("Submesh {} submeshes: ({} verts) using material {}", 
			submeshCounter++, sub.indices.size(), sub.mat->name), LogType::Comment);
	}
#else
	LOG(fmt::format("Loaded mesh \"{}\" from file {}: {} verts, {} submeshes."
		, name, location, vertices.size(), subMeshes.size()), LogType::Comment);
#endif
	return true;
}
