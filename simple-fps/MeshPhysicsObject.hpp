#pragma once
#include "PhysObj.hpp"

class MeshPhysicsObject :
	public PhysicsObject {
public:
	MeshPhysicsObject(MeshManager * model, bool isStatic);
	~MeshPhysicsObject();
	void load_static_collision_shape(const MeshManager * model);
	void load_dynamic_collision_shape(const MeshManager * model);
private:
	MeshManager * model;
	btConvexHullShape * unoptimized_hull;
	btShapeHull * hull_optimizer;
	btConvexHullShape * hull;

	btTriangleMesh * triangle_mesh;
	btBvhTriangleMeshShape * triangle_mesh_shape;

};
