#pragma once
#include "ResourceManager.hpp"

class ShaderManager : public ResourceManager<ShaderManager> {
public:
	ShaderManager(std::string name);
	~ShaderManager();

	typedef std::map<std::string, ShaderManager*> ShaderMap;
	unsigned int shaderProgram;

	GLuint get_uniform(std::string name);
	std::string dir() override;

protected:
	bool loadResource(std::string name, bool mt) override;

private:
	GLint compileShader(unsigned int vertexShader, unsigned int fragmentShader, char infoLog[]) const;
	std::map<std::string, GLuint> uniforms;
};
