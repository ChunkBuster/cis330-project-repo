#pragma once
#include "fwd.h"
#include "LinearMath/btVector3.h"

class PhysicsObject {
public:
	explicit PhysicsObject(bool isStatic);
	PhysicsObject(glm::vec3 origin, float radius);
	~PhysicsObject();

	bool dynamic;


	btCollisionShape * collision_shape;
	btDefaultMotionState * motion_state;
	btScalar mass;
	btVector3 inertia;
	btRigidBody * rb;

	void reposition(btVector3 position, btQuaternion orientation) const;
private:
	void load_dynamic_sphere(glm::vec3 origin, float radius);
};
