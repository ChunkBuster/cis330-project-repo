﻿#pragma once
#include "Renderer.hpp"

class game {
public:
	static void add_mesh(renderer * rend);
	static void remove_mesh(renderer * rend);
	static void change_map(std::string map);
	static void render();

private:
	static game * Instance;
	renderer * map;
	std::vector<renderer*> meshes;
};
