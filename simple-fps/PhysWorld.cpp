﻿#include "stdafx.h"
#include "Logger.hpp"
#include "Renderer.hpp"
#include "Transform.hpp"
#include "PhysWorld.hpp"

PhysWorld * PhysWorld::Instance = new PhysWorld();

PhysWorld::PhysWorld() : broadphase(new btDbvtBroadphase()),
                         collisionConfiguration(new btDefaultCollisionConfiguration()),
                         dispatcher(new btCollisionDispatcher(collisionConfiguration)),
                         solver(new btSequentialImpulseConstraintSolver), rendererMap() {
	//Mesh collisions
	btGImpactCollisionAlgorithm::registerAlgorithm(dispatcher);
	dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);
	dynamicsWorld->setGravity(btVector3(0, -10, 0));
	LOG("Instantiated physics world", LogType::Message);
}


PhysWorld::~PhysWorld() {
	delete dynamicsWorld;
	delete solver;
	delete dispatcher;
	delete collisionConfiguration;
	delete broadphase;
}


void PhysWorld::register_rigidbody(btRigidBody * body) {
	Instance->dynamicsWorld->addRigidBody(body);
	LOG("Registered new rigidbody", LogType::Comment);
}

void PhysWorld::register_renderer(renderer * rend, btRigidBody * body) {
	Instance->rendererMap[body] = rend;
}


void PhysWorld::step() {
	Instance->dynamicsWorld->stepSimulation(1 / 60.f, 10);
	for (auto pair : Instance->rendererMap) {
		Instance->rendererMap[pair.first]->transf->set_to_rb(*pair.first);
	}
}
