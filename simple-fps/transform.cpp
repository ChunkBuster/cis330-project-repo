﻿#include "stdafx.h"
#include "Transform.hpp"
#include "Conversion.hpp"

transform::transform(transform * parent) : transform() {
	this->parent = parent;
}

transform::transform() : parent(), scale(1), rotation(), position(0), skew(0), perspective(0) {
}

glm::vec3 transform::global_position() const {
	return glm::vec3(get_matrix_global() * glm::vec4(1));
}

glm::vec3 transform::up() const {
	return normalize(glm::vec3(0, 1, 0) * rotation);
}

glm::vec3 transform::down() const {
	return normalize(glm::vec3(0, -1, 0) * rotation);
}

glm::vec3 transform::right() const {
	return normalize(glm::vec3(1, 0, 0) * rotation);
}

glm::vec3 transform::back() const {
	return normalize(glm::vec3(0, 0, -1) * rotation);
}

glm::vec3 transform::left() const {
	return normalize(glm::vec3(-1, 0, 0) * rotation);
}

glm::vec3 transform::forward() const {
	return normalize(glm::vec3(0, 0, 1) * rotation);
}

glm::mat4 transform::get_matrix() const {
	glm::mat4 outMat(1);
	outMat *= toMat4(rotation);
	outMat = translate(outMat, position);
	outMat = glm::scale(outMat, scale);
	return outMat;
}

glm::mat4 transform::get_matrix_global() const {
	glm::mat4 outMat(1);

	transform * currParent = parent;
	std::vector<transform *> parent_chain;
	while (currParent != nullptr) {
		parent_chain.push_back(currParent);
		currParent = currParent->parent;
	}
	for (int i = parent_chain.size() - 1; i >= 0; i--)
		outMat *= parent_chain[i]->get_matrix();
	outMat *= get_matrix();
	return outMat;
}

void transform::set_matrix(glm::mat4 matrix) {
	decompose(matrix, scale, rotation, position, skew, perspective);
}

void transform::set_to_rb(btRigidBody & body) {
	btTransform bt;
	glm::mat4 outMat(1);

	body.getMotionState()->getWorldTransform(bt);

	glm::quat rot = bt_quat_to_glm(bt.getRotation());
	glm::vec3 pos = bt_v3_to_glm(bt.getOrigin());

	outMat *= toMat4(rot);
	outMat = translate(outMat, pos);
	outMat = glm::scale(outMat, scale);

	set_matrix(outMat);
}
