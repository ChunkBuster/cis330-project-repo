#include "stdafx.h"
#include "game.hpp"
// Include GLEW for Pointer Management for GLFW

#include "InputManager.hpp"
//Include user-defined header
#include "MeshManager.hpp"
#include "Renderer.hpp"
#include "Weapon.hpp"

#include "PhysObj.hpp"
#include "PhysWorld.hpp"
#include "MeshPhysicsObject.hpp"

#define BULLET_PHYSICS 1

#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

int main() {

	setlocale(LC_ALL, "");
	//Init GLFW. We need to do this before any GLFW calls may be made.
	if (!glfwInit()) {
		std::cerr << "glfw could not init!" << std::endl;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	//Required for macs
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	//Get a pointer to a window object, created with the specified params
	GLFWwindow * window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "FPS", nullptr, nullptr);

	//If the window cannot be created, return a simple error and finish.
	if (window == nullptr) {
		std::cerr << "Failed to create GLFW window" << std::endl;
		//Terminate the glfw sessio
		glfwTerminate();
		//Return error code
		return -1;
	}

	//Set the active context to the current window
	glfwMakeContextCurrent(window);

	//vsync
	glfwSwapInterval(1);

	std::vector<GLclampf> clearColor = {GLclampf(0), GLclampf(0), GLclampf(0), GLclampf(1)};
	// Initialize GLEW
	//GLEW handles function pointer for OpenGL. I'm using it instead of GLAD because I am a shitboy.
	glewExperimental = true;
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}

	//Defines how large the viewport being rendered to actually is (as GLFW doesn't directly interface with OpenGL)
	//glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
	TIME_CALL(game::change_map("cs_office"));

	InputManager::setCursorLock(window, false);
	InputManager::init_ui(window);

	game::add_mesh(InputManager::player_gun->rend);

	/*
	renderer * rock = mesh_manager::get_resource("rock", false)->get_renderer();
	phys_obj * rock_obj = new phys_obj(mesh_manager::get_resource("rock", false), false);
	Phys_World::register_renderer(rock, rock_obj->rb);
	game::add_mesh(rock);*/
	// Get a handle for our "MVP" uniform


	//std::vector<GLclampf> clearColor = { GLclampf(0), GLclampf(0.4), GLclampf (0.7), GLclampf(1)};

#if BULLET_PHYSICS

	//Init floor
	//btCollisionShape* groundShape = new btStaticPlaneShape(btVector3(0, 1, 0), 1);
	//btConvexHullShape* groundShape = new btConvexHullShape((const btScalar*)(&(v.xyzw[0])), glmesh->m_numvertices, sizeof(GLInstanceVertex));
	PhysicsObject * ground_object = new MeshPhysicsObject(MeshManager::get_resource("cs_office", false), true);
#endif
	while (!glfwWindowShouldClose(window)) {
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glClearColor(clearColor[0], clearColor[1], clearColor[2], clearColor[3]);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Polls events
		glfwPollEvents();

		//Render the game
		game::render();

		//Draw imgui
		InputManager::draw_imgui();

		//Detect keypresses
		InputManager::process_input(window);

		PhysWorld::step();

		//rock->transf->set_to_rb(*rock_obj->rb);
		//rock->transf->translation = glm::vec3(trans.get.getX(), transf.getY(), transf.getZ());
		//rock->transf->rotation = glm::quat(trans.getRotation().getW(), -trans.getRotation().getX(),
		//                                  -trans.getRotation().getY(), -trans.getRotation().getZ());
		//LOG(fmt::format("Sphere height: {}", trans.getOrigin().getY()), LogType::Comment);

		//Updates the window
		glfwSwapBuffers(window);
	}

#if BULLET_PHYSICS
	// Delete bullet stuff
#endif
	//Frees all resources
	glfwTerminate();
	return 0;
}

game * game::Instance = new game();

void game::add_mesh(renderer * rend) {
	Instance->meshes.push_back(rend);
}

void game::change_map(std::string map_name) {
	LOG(fmt::format("Changing map to {}...", map_name), LogType::Message);
	delete Instance->map;
	Instance->map = MeshManager::get_resource(map_name, false)->get_renderer();
	LOG(fmt::format("map {} loaded", map_name), LogType::Message);
}

void game::render() {

	float time = glfwGetTime();
	Instance->map->render();
	for (auto m : Instance->meshes)
		m->render();
}
