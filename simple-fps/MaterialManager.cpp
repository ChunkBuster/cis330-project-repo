#include "stdafx.h"
#include "MaterialManager.hpp"

material_manager::material_manager(std::string name) :
	ResourceManager<material_manager>(name), amb_map(nullptr), diffus_map(nullptr),
	emissive_map(nullptr), spec_map(nullptr), normal_map(nullptr),
	bump_map(nullptr), stencil_map(nullptr), anisotropy_map(nullptr) {
}


material_manager::~material_manager() {
}

std::string material_manager::dir() {
	return ResourceManager<material_manager>::dir() + "materials/";
}

bool material_manager::loadResource(std::string name, bool mt) {
	return true;
}
