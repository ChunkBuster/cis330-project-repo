﻿#pragma once
#include "fwd.h"

namespace user_input
{
	struct ExampleAppConsole;
}

#define LOG2(x, y) Logger::log_imp(x, y, __LINE__, __func__, __FILE__)
#define LOG3(x, y, z) Logger::log_imp(x, y, z, __LINE__, __func__, __FILE__)
#define LOG(...) CALL_OVERLOAD(LOG, __VA_ARGS__)

class Logger {
public:
	Logger();
	~Logger();
	static void log_imp(std::string message, LogType LogType, int line, const char * func, const char * file);
	static void log_imp(std::string message, LogType logType, std::string glyph, int line, const char * func,
	                    const char * file);

	static void set_log_level(int level);
	static void draw_console();

private:
	static Logger * _Instance;
	user_input::ExampleAppConsole * _console;
	int _logLevel = 0;
	int _messages = 0;
	int _warnings = 0;
	int _errors = 0;
};
