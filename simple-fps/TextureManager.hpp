#pragma once
#include "ResourceManager.hpp"

class texture_manager : public ResourceManager<texture_manager> {
public:
	typedef std::map<std::string, texture_manager*> ImageMap;

	texture_manager(std::string name);
	~texture_manager();
	void init_image(bool mt = false);
	bool free_data() const;

	unsigned int texture;

	int width;
	int height;
	int channels;


protected:
	//void parse_image(unsigned char *imageData);
	std::string dir() override;
	bool loadResource(std::string name, bool mt) override;
	unsigned char * imageData;

private:
	bool initialized = false;
};
