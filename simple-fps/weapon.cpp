#include "stdafx.h"
#include "Weapon.hpp"
#include "MeshManager.hpp"
#include "InputManager.hpp"
#include "Renderer.hpp"
#include "PhysWorld.hpp"
#include "PhysObj.hpp"
#include "game.hpp"

Weapon::Weapon() : rend(MeshManager::get_resource("guns2", false)->get_renderer()),
                   properties(glm::vec3(-0.25f, -0.5f, -0.45f)),
                   firing(false), tip(rend->transf) {
	rend->transf->scale = glm::vec3(0.1);
	rend->transf->rotation *= angleAxis(glm::radians(-90.0f), glm::vec3(0, 1, 0));
	rend->transf->position = properties.start_pos;
	rend->transf->parent = &InputManager::player_transform;

	tip.parent = rend->transf;
	tip.position = glm::vec3(-5, 2.2, 1.f);
	LOG(fmt::format("Equipped Weapon \"Rifle\""), LogType::Message);
}

void Weapon::update_gun_values() const {
	const GLuint lightUniformLocation = ShaderManager::get_resource("StandardShading", false)->get_uniform(
		"LightPosition_worldspace");
	glUniform3fv(lightUniformLocation, 1, value_ptr(tip.global_position()));
	const GLuint lightPowerUniformLocation = ShaderManager::get_resource("StandardShading", false)->
		get_uniform("LightPower");
	glUniform1f(lightPowerUniformLocation, brightness);
	const GLuint lightColorUniformLocation = ShaderManager::get_resource("StandardShading", false)->
		get_uniform("LightColor");
	glUniform3fv(lightColorUniformLocation, 1, value_ptr(properties.color));
};

void Weapon::poll(int key, int action) {
	if (key == GLFW_MOUSE_BUTTON_1) {

		if (action == GLFW_PRESS) {
			firing = true;
			static bool coroutine_running = false;
			if (!coroutine_running) {
				renderer * rock = MeshManager::get_resource("Ball", false)->get_renderer();
				rock->transf->scale = glm::vec3(0.25f);

				PhysicsObject * rock_obj = new PhysicsObject(tip.global_position(), 0.25);
				//rock_obj->reposition(glm_v3_to_bt(rend->transf->position), glm_quat_to_bt(rend->transf->rotation));
				PhysWorld::register_renderer(rock, rock_obj->rb);
				game::add_mesh(rock);

				spawn(InputManager::ioService, [&](boost::asio::yield_context yield) {
					coroutine_running = true;
					double startTime = glfwGetTime();
					float scalar = 0;
					double deltaTime = 0;

					auto update_values = [&deltaTime, &scalar, startTime, this]() {
						deltaTime = glfwGetTime() - startTime;

						brightness = lerp_floats(0, properties.brightness, scalar);
						rend->transf->position = lerp_vec3(properties.start_pos,
						                                   properties.start_pos - glm::vec3(-properties.kickback, 0, 0), scalar);
						update_gun_values();
					};

					while (firing) {
						scalar = glm::mod(deltaTime / properties.fire_rate, 1.0);
						update_values();
						boost::asio::steady_timer t(InputManager::ioService, std::chrono::milliseconds(1));
						t.async_wait(yield);
					}
					while (scalar > 0) {
						scalar -= properties.fire_rate;
						update_values();
						boost::asio::steady_timer t(InputManager::ioService, std::chrono::milliseconds(1));
						t.async_wait(yield);
					}
					coroutine_running = false;
					return;
				});
			}
		}

		if (action == GLFW_RELEASE) {
			firing = false;
		}
	}
}

Weapon::~Weapon() {
}
