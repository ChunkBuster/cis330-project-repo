#include "stdafx.h"
#include "SKSMath.hpp"

float normalizedSin(float t) {
	return (((float)std::sin(t) + 1.0f) * 0.5f);
}

SKSMath::SKSMath() {
}


SKSMath::~SKSMath() {
}
