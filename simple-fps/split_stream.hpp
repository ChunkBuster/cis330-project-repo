#if SPLIT_STREAM
#pragma once
#include <sstream>
#include "stream_override.hpp"

class split_stream 
{
public:
	split_stream();

	stream_override ovr;

	static split_stream *split;
	std::stringstream of_stream_split;
	std::streambuf *old_cout;
	std::ostream backup_cout;

	static void split_cout();
	static void join_cout();
};
#endif
