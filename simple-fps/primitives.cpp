#include "stdafx.h"
#include "primitives.hpp"

static const float TriangleVerts_Data[] =
{
	-0.5f, -0.5f, 0.0f,
	0.5f, -0.5f, 0.0f,
	0.0f, 0.5f, 0.0f
};

const float * Primitives::TriangleVerts = TriangleVerts_Data;
